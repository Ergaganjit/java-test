

public abstract class Shape {
	 public abstract double  calculateVolume();
	 public abstract double 	 printDetails();
}


import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		

		Scanner keyboard = new Scanner(System.in);
		int choice = 0;
		
		while (choice != 4) {
			// 1. show the menu
			
			showMenu();
	
			// 2. get the user input
			System.out.println("Enter a number: ");
			choice = keyboard.nextInt();
			
			// 3. DEBUG: Output what the user typed in 
			System.out.println("You entered: " + choice);
			
		if(choice==1)
		{
			System.out.println("Enter Dimentions of Triangle ");
			System.out.println("Enter base of Triangle: ");
			double base  = keyboard.nextDouble();
			
			
			System.out.println("Enter height of Triangle: ");
			double height  = keyboard.nextDouble();
			
			Shape obj=new Triangle(base,height);
			obj.calculateVolume();
			obj.printDetails();
			/*ArrayList<Shape> shape = new ArrayList<Shape>();
			shape.add(Triangle);*/
		
			
		}
		else if (choice==2)
		{
			
			System.out.println("Enter Dimentions of Square ");
			System.out.println("Enter side of Square: ");
			double side  = keyboard.nextDouble();
			Shape obj=new Square(side);
			obj.calculateVolume();
			obj.printDetails();
			/*ArrayList<Shape> shape = new ArrayList<Shape>();
			shape.add(Square);*/
			
			
		}
		else if (choice==3)
		{
			System.out.println("Exiting From Program");
			System.exit(0);
		}
		
		}
		
		
		
	}
	
	public static void showMenu() {
		System.out.println("AREA GENERATOR");
		System.out.println("==============");
		System.out.println("1. Triangle");
		System.out.println("2. Square");
		System.out.println("3. Rectangle");
		System.out.println("4. Exit");
	}
	
	
	
	

}

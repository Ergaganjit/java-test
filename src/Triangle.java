import javax.jws.soap.SOAPBinding;

public class Triangle extends Shape implements ThreeDimensionalShapeInterface  {
	
	private  double base,height;

	private double area;
	public Triangle(double base,double height) {
        this.base = base;
        this.height= height;
        
    }
	
	public double getBase() {
		return base;
	}
	public void setBase(double base) {
		this.base = base;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	@Override
	public double calculateVolume() {
		// TODO Auto-generated method stub
		  area=1/2*base*height; 
         
		return 0;
	}

	@Override
	public double printDetails() {
		// TODO Auto-generated method stub
		
		System.out.println("Area of Tiangle is :"+area);
		return 0;
	}

}

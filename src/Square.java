
public class Square extends Shape implements ThreeDimensionalShapeInterface  {
	double side;
	double area;
	
	public Square(double side) {
        this.side = side;
       
        
    }

	public double getSide() {
		return side;
	}

	public void setSide(double side) {
		this.side = side;
	}

	@Override
	public double calculateVolume() {
		// TODO Auto-generated method stub
		 area=side*side;
		return 0;
	}

	@Override
	public double printDetails() {
		// TODO Auto-generated method stub
		System.out.println("Area of sqaure is "+area);
		return 0;
	}

}
